/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.launcher;

import atmsystem.accounts.Checking;
import atmsystem.accounts.Savings;
import atmsystem.menu.MainMenu;
import java.util.Scanner;

/**
 *
 * @author Archimage
 */
public class ATMSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int id = 1;
        String name = "John Doe";
        Checking checking = new Checking(id, name, 500.0);
        Savings saving = new Savings(id, name, 1000.0);
        try (Scanner input = new Scanner(System.in)) {
            MainMenu mainMenu = new MainMenu(checking, saving, input);
            mainMenu.startMenu();
        }
    } 
}
