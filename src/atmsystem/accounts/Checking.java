/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.accounts;

/**
 *
 * @author Archimage
 */
public class Checking extends AbstractAccount{
    
    private final double monthlyFee = 3.0;
    public Checking(int id, String name, double balance) {
        super(id, name, balance, "Checking");
    }
    
    @Override
    public void printSpecificMonthlyReport(){
        super.chargeMonthlyFee(monthlyFee);
        System.out.printf("Monthly fee: %.2f %n" +
                "Balance after monthly fee: %.2f %n", 
                getMonthlyCharge(),
                super.getBalance());
    }
    
    @Override
    public double getMonthlyCharge(){
        return monthlyFee;
    }
    
}
