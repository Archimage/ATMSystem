/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.accounts;

/**
 *
 * @author Archimage
 */
public class Savings extends AbstractAccount{
    
    //Constants
    private double monthlyCharge;
    private final double minBalance = 1000;
    private final double interestRate = 0.03;
    
    public Savings(int id, String name, double balance){
        super(id, name, balance, "Savings");
    }
    
    //Determines the monthly charge based on the current balance.
    private void setMonthlyCharge(){
        monthlyCharge = (super.getBalance() < minBalance) ? 25.0 : 0.0;
    }
    
    //Returns the annual interest rate for the savings account.
    public double getAnnualInterest(){
        return interestRate;
    }
    
    @Override
    //Prints the monthly report for the savings account.
    public void printSpecificMonthlyReport(){
        setMonthlyCharge();
        super.chargeMonthlyFee(monthlyCharge);
        System.out.printf("Penalty: %.2f %n" +
                "Balance after Penalty: %.2f %n" +
                "Annual interest rate: %.2f %n", 
                getMonthlyCharge(),
                super.getBalance(),
                getAnnualInterest());
        //Add interest to balance
        super.addInterest(interestRate/12);
        System.out.printf("Interest Earned: %.2f %n" +
                "Balance after interest: %.2f %n", 
                super.getInterestAmount(),
                super.getBalance());
    }
    
    @Override
    //Returns the amount charged to the account.
    public double getMonthlyCharge(){
        return monthlyCharge;
    }
}
