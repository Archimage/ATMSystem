/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.accounts;

import atmsystem.transaction.Transaction;
import atmsystem.transaction.TransactionPrinter;
import java.util.ArrayList;

/**
 *
 * @author Archimage
 */
public abstract class AbstractAccount {
    
    //Private fields.
    private final int id;
    private final String type;
    private final String name;
    private double balance;
    private static final double USAGE_FEE = 1.0;
    private double interestAmount;
    private final ArrayList<Transaction> transactionLog;
    
    protected AbstractAccount(int id, String name, double balance, String type){
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.type = type;
        transactionLog = new ArrayList<>();
    }
    
    //Returns account id.
    public int getID(){
        return id;
    }
    
    //Returns account type.
    public String getType(){
        return type;
    }
    
    //Returns the name of the owner.
    public String getName(){
        return name;
    }
    
    //Returns the balance in the account.
    public double getBalance(){
        return balance;
    }
    
    public void deposit(double amount) {
        balance += amount;
        applyFee();
        transactionLog.add(
                new Transaction('D', amount, USAGE_FEE, getBalance(), ""));
    }
    
    public void withdraw(double amount) {
        balance -= amount;
        applyFee();
        transactionLog.add(
                new Transaction('W', amount, USAGE_FEE, getBalance(), ""));
    }
    
    public void transferIn(double amount, String origin) {
        balance += amount;
        transactionLog.add(new Transaction('T', amount, 0, getBalance(), 
                "Transfer in from " + origin));
    }
    
    public void transferOut(double amount, String target) {
        balance -= amount;
        applyFee();
        transactionLog.add(new Transaction('T', amount, USAGE_FEE, getBalance(), 
                "Transfer out to " + target));
    }
    
    //Charges transaction fee.
    private void applyFee(){
        balance -= USAGE_FEE;
    }
    
    //Adds the amount of interest earned to the balance.
    protected void addInterest(double interest) {
        interestAmount = balance * interest;
        balance += interestAmount;
    }
    
    //Returns the amount of interest earned
    protected double getInterestAmount(){
        return interestAmount;
    }
    
    //Deducts a monthly fee from the balance.
    protected void chargeMonthlyFee(double monthlyFee) {
        balance -= getMonthlyCharge();
    }
    
    //Sets up a template for the monthly report
    public void printMonthlyReport(){
        //Generic report header for all types.
        System.out.print(getType() + " Monthly Statement" +
                System.lineSeparator() + 
                "Account owner: " + getName() + System.lineSeparator());
        //Print information specific to the type of account.
        printSpecificMonthlyReport();
        //Print transaction log.
        TransactionPrinter.printHeader();
        for(Transaction entry: transactionLog){
            TransactionPrinter.formatTransaction(entry);
        }
        System.out.println();
    }
    
    //Abstract methods
    public abstract double getMonthlyCharge();
    public abstract void printSpecificMonthlyReport();
}
