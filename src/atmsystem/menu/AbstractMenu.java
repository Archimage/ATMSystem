/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.menu;

import java.util.Scanner;

/**
 *
 * @author Archimage
 */
public abstract class AbstractMenu {
    
    protected final Scanner input;
    protected final static String RETRY_MSG = "Hit Enter key to retry.";
    protected final static String RETURN_MSG = "Hit Enter key to return to "
            + "the Main Menu";
    
    protected AbstractMenu(Scanner input){
        this.input = input;
    }
    
    //Method to accept char input.
    protected char getResponse(){
        //Scanner.nextLine() will consume an empty string after a
        //Scanner.nextDouble() because the /n string is not consumed by the
        //nextDouble method.
        return input.next().toLowerCase().charAt(0);
    }

    //Forcing user to only input Enter key.
    protected void forceEnter(String message){
        String readString;
        do{
            System.out.println(message);
            readString = input.nextLine();
        } while(!readString.isEmpty());
    }
                    
}
