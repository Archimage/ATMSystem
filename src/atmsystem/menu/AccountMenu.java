/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.menu;

import atmsystem.accounts.AbstractAccount;
import atmsystem.accounts.Checking;
import atmsystem.accounts.Savings;
import java.util.Scanner;

/**
 *
 * @author Archimage
 */
public class AccountMenu extends AbstractMenu{
    
    private final Checking checking;
    private final Savings savings;
    private boolean isValid;
    
    public AccountMenu(Checking checking, Savings savings, Scanner input){
        super(input);
        this.checking = checking;
        this.savings = savings;
        isValid = true;
    }
    
    //Account menu logic
    private AbstractAccount showMenu(){
        AbstractAccount account = null;
        switch(getResponse()){
                case 'c':
                    account = checking;
                    isValid = true;
                    break;
                case 's':
                    isValid = true;
                    account = savings;
                    break;
                default:
                    isValid = false;
                    System.out.println("Error. Invalid input letter!");
                    super.forceEnter(AbstractMenu.RETRY_MSG);
                    break;
            }
        return account;
    }
    
    //Print account options to user
    private void printMenu() {
        System.out.println("Account menu" + System.lineSeparator() +
                " C: Checkings account" + System.lineSeparator() +
                " S: Savings account");
    }
    
    //Deposit or withdraw
    public AbstractAccount printAccountTransaction(String action) {
        AbstractAccount account;
        do{
            printMenu();
            System.out.print("Select an account for a " + action + 
                    " transaction: ");
            account = showMenu();
        } while(!isValid);
        return account;
    }
    
    //Transaction
    public AbstractAccount printAccountTransfer(String type, String target){
        AbstractAccount account;
        do{
            printMenu();
            System.out.print("Select the account to " + type + " transfer " +
                    target + " : ");
            account = showMenu();
        } while(!isValid);
        return account;
    }
}
