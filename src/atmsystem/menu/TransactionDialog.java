/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.menu;

import atmsystem.accounts.AbstractAccount;
import java.util.Scanner;

/**
 *
 * @author Archimage
 */
public class TransactionDialog extends AbstractMenu{
    
    public TransactionDialog(Scanner input){
        super(input);
    }
    
    public double printDialog(AbstractAccount account, 
            String action1, String action2) {
        double amount;
        do{
            System.out.print("Enter the amount to " + action1 + " : ");
            amount = input.nextDouble();
        } while(!isValid(amount, account, action1));
        System.out.println(amount + " was sucessfully " + action2 + " your " + 
                account.getType() + " account");
        System.out.println();
        return amount;
    }
    
    public double printTransferDialog(AbstractAccount source, 
            AbstractAccount target) {
        double amount;
        do{
            System.out.print("Enter the amount to transfer: ");
            amount = input.nextDouble();
        } while(!isValid(amount, source, "withdraw"));
        System.out.println(amount + " was sucessfully transferred from " + 
                source.getType() + " account to " + target.getType() + 
                " account");
        System.out.println();
        return amount;
    }
    
    //Check validity of input
    private boolean isValid(double amount, AbstractAccount account, 
            String action){
        //Filter negative value
        if(amount < 0){
            System.out.println("Error: negative value!");
            return false;
        }
        
        //Filter insufficient funds.
        if(amount > account.getBalance() && "withdraw".equalsIgnoreCase(action)){
            System.out.println("Error: insufficient funds!");
            return false;
        }
        else{
            return true;
        }
    }
}
