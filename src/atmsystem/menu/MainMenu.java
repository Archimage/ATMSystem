/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.menu;

import atmsystem.accounts.AbstractAccount;
import atmsystem.accounts.Checking;
import atmsystem.accounts.Savings;
import java.util.Scanner;

/**
 *
 * @author Archimage
 */
public class MainMenu extends AbstractMenu{
    
    //Fields
    private boolean run;
    private final AccountMenu accountMenu;
    private final TransactionDialog dialog;
    private final AbstractAccount checking, savings;
    
    public MainMenu(Checking checking, Savings savings, Scanner input){
        super(input);
        run = true;
        accountMenu = new AccountMenu(checking, savings, input);
        dialog = new TransactionDialog(input);
        this.checking = checking;
        this.savings = savings;
    }
    
    //Print operation options to user.
    private void printMenu() {
        System.out.print("Main Menu" + System.lineSeparator() +
                " D: deposit" + System.lineSeparator() +
                " W: withdraw" + System.lineSeparator() +
                " T: transfer" + System.lineSeparator() +
                " Q: quit" + System.lineSeparator() +
                "Enter D, W, or T to start transaction or Q to quit: ");
    }
    
    //Main menu logic
    private boolean showMenu(){
        printMenu();
        AbstractAccount account;
        switch(getResponse()){
            case 'd':
                System.out.println();
                account = accountMenu.printAccountTransaction("deposit");
                account.deposit(dialog.printDialog(account, 
                        "deposit", "added to"));
                run = true;
                break;
            case 'w':
                System.out.println();
                account = accountMenu.printAccountTransaction("withdrawal");
                account.withdraw(dialog.printDialog(account, 
                        "withdraw", "withdrawn from"));
                run = true;
                break;
            case 't':
                System.out.println();
                //Determine source and target account.
                AbstractAccount sourceAccount = accountMenu
                        .printAccountTransfer("source", "from");
                AbstractAccount targetAccount;
                //Prevent source and target to be the same
                do{
                    targetAccount = accountMenu
                            .printAccountTransfer("destination", "to");
                    //Print error message.
                    if(sourceAccount.equals(targetAccount)){
                        System.out.println("Error: source and destination "
                                + "account cannot be same.");
                    }
                } while(sourceAccount.equals(targetAccount));
                
                double transferAmount = dialog.printTransferDialog(
                        sourceAccount, targetAccount);
                //Execute transfer.
                sourceAccount.transferOut(transferAmount, targetAccount.getType());
                targetAccount.transferIn(transferAmount, sourceAccount.getType());
                run = true;
                break;
            case 'q':
                System.out.println();
                run = false;
                checking.printMonthlyReport();
                savings.printMonthlyReport();
                break;
            default:
                run = true;
                System.out.println("Error: invalid input letter!");
                super.forceEnter(AbstractMenu.RETRY_MSG);
                break;
        }
        return run;
    }
    
    public void startMenu(){
        while(run){
            run = showMenu();
        }
        
    }
    
}
