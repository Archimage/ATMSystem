/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.transaction;

import java.time.LocalDate;

/**
 *
 * @author Archimage
 */
public class Transaction {
    
    /*
    Immutable object: no setters.
    */
    
    //Fields
    private final LocalDate date;
    private final char type;
    private final double amount;
    private final double fee;
    private final double balance;
    private final String comments;
    
    public Transaction(char type, double amount, double fee, double balance, 
            String comments){
        this.type = type;
        this.amount = amount;
        this.fee = fee;
        this.balance = balance;
        this.comments = comments;
        date = LocalDate.now();
    }
    
    public char getType(){
        return type;
    }
    
    public double getAmount(){
        return amount;
    }
    
    public double getFee(){
        return fee;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getBalance() {
        return balance;
    }

    public String getComments() {
        return comments;
    }
}
