/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmsystem.transaction;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author Archimage
 */
public class TransactionPrinter {
    
    //Defaults
    private static final String DATE_PATTERN = "M/d/yyyy";
    private static final DateTimeFormatter DATE_FORMAT = 
            DateTimeFormatter.ofPattern(DATE_PATTERN);
    
    public TransactionPrinter(){}
    
    //Default
    public static void formatTransaction(Transaction transaction) {
        formatTransaction(transaction, DATE_FORMAT);
    }
    
    //Custom date format.
    public static void formatTransaction(Transaction transaction, 
            DateTimeFormatter dateFormat) {
        System.out.printf("%-15s %-7c %-10.2f %-7.2f %-10.2f %-30s %n", 
                transaction.getDate().format(dateFormat),
                transaction.getType(),
                transaction.getAmount(),
                transaction.getFee(),
                transaction.getBalance(),
                transaction.getComments());
    }
    
    public static void printHeader(){
        System.out.println("Transactions: ");
        System.out.printf("%-15s %-7s %-10s %-7s %-10s %-10s %n",
                "Date", "Type", "Amount", "Fee", "Balance", "Comments");
    }
}
